<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'MED.' . $_EXTKEY,
	'Maps',
	array(
		'Maps' => 'list',
		
	),
	// non-cacheable actions
	array(
		'Maps' => '',
		
	)
);
